package domain;

public class BreakthroughInternetCommunicator implements NavegadorInternet {
    @Override
    public void ixibirPagina() {
        System.out.println("Ixibindo Pagina...");
    }

    @Override
    public void adicinarNovaAba() {
        System.out.println("Adicionando nova aba...");
    }

    @Override
    public void atualizarPagina() {
        System.out.println("Atualizando pagina...");
    }
}
