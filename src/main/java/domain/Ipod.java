package domain;

public class Ipod implements ReprodutorMusical{
    @Override
    public void tocar() {
        System.out.println("tocando...");
    }

    @Override
    public void pausar() {
        System.out.println("Pausando...");
    }

    @Override
    public void selecionarMusica() {
        System.out.println("Selecionando musica");
    }
}
